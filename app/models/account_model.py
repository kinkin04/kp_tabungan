from sqlalchemy import Column, String, BigInteger, Text, DateTime, Numeric
from datetime import datetime
from service import Base

class Account(Base):
    __tablename__ = 'account'
    id =Column(BigInteger, primary_key=True)
    name =Column(String(50), nullable=False)
    no_identity =Column(String(50), nullable=False)
    phone =Column(String(50), nullable=False)
    account_number =Column(String(30))
    balance =Column(Numeric(28, 2))
    is_active =Column(String(1))
    address =Column(Text)
    created_at =Column(DateTime, default=datetime.now())