from fastapi import APIRouter

from .account_api import *

api_router = APIRouter()

api_router.include_router(account_api.router, prefix='/acc', tags=['Tabungan'])