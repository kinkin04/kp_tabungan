import random


def response(message_id: str, status:str, data=None):
    response = {
        "message_id" : message_id,
        "status" : status,
        "data" : data
    }

    return response

def generate_account_number():
    account_number = "325"
    for _ in range(7):
        account_number += str(random.randint(0, 9))
    return account_number