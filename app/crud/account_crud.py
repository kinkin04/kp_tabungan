import uuid
from fastapi.encoders import jsonable_encoder
from sqlalchemy.future import select
from sqlalchemy import update, or_, and_, desc
from datetime import datetime
from service import response, generate_account_number
from models import account_model, transaction_model
from schema import RegistAcc, postTransaction


async def create_regist_acc(data: RegistAcc, session) -> response:
    try:
        resVal = await valAccExist(data.no_identity, session)
        if resVal:
            raise Exception(f"NIK sudah terdafar dengan nomor rekening lain")

        account_number = generate_account_number()
        data = account_model.Account(
            name=data.name,
            no_identity=data.no_identity,
            phone=data.phone,
            account_number=account_number,
            balance=0,
            is_active="T",
            address="Desa Ampel Kec. Wuluhan Kab. Jember - Jawa Timur",
            created_at=datetime.now(),
        )
        session.add(data)
        await session.commit()
        return response(
            "200",
            "Pendaftaran nomor rekening berhasil",
            {"account_number": account_number},
        )

    except Exception as e:
        return response("400", str(e), None)


async def data_account(no_identity: str, phone: None, session) -> response:
    try:
        acc = account_model.Account
        query_stmt = select(acc).filter(
            or_(acc.no_identity == no_identity, acc.phone == phone)
        )
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)
    except Exception as e:
        return response("400", str(e), None)


async def deposit_balance(data: postTransaction, session) -> response:
    try:
        # update balance account
        _acc = await acc_exist(data.account_number, session)
        if _acc in (None, ""):
            raise Exception(f"Nomor rekening {data.account_number} tidak ditemukan")

        if data.nominal <= 0 or data.nominal < 5000:
            raise Exception(f"Maaf, masukkan nominal yang sesuai (lebih dari Rp.5000)")

        get_current_balance = await balance_account(data.account_number, session)

        acc = account_model.Account

        balacnce_amount = data.nominal + get_current_balance["data"]

        update_balance = (
            update(acc)
            .where(acc.account_number == data.account_number)
            .values(balance=balacnce_amount)
        )

        trx = transaction_model.Transaction
        data = trx(
            idkey=str(uuid.uuid4()),
            type="Credit",
            account_number=data.account_number,
            mutation="C",
            nominal=float(data.nominal),
            created_at=datetime.now(),
        )
        session.add(data)

        await session.execute(update_balance)
        await session.commit()
        return response(
            "200",
            f"Setor tunai dengan nomor rekening {data.account_number} berhasil, saldo sekarang Rp. {balacnce_amount}",
            balacnce_amount,
        )

    except Exception as e:
        return response("400", str(e), None)


async def cash_withdrawal(data: postTransaction, session) -> response:
    try:
        # update balance account after withdraw
        _acc = await acc_exist(data.account_number, session)
        if _acc in (None, ""):
            raise Exception(f"Nomor rekening {data. account_number} tidak ditemukan")

        acc = account_model.Account

        get_current_balance = await balance_account(data.account_number, session)
        if get_current_balance["data"] < data.nominal:
            raise Exception("Saldo tidak cukup")

        if data.nominal <= 0 or data.nominal < 5000:
            raise Exception(f"Maaf, nominal yang anda tarik harus lebih dari Rp.5000")

        balance_amount = float(get_current_balance["data"]) - float(data.nominal)
        trx = transaction_model.Transaction
        data = trx(
            idkey=str(uuid.uuid4()),
            type="Debit",
            account_number=data.account_number,
            mutation="D",
            nominal=data.nominal,
            created_at=datetime.now(),
        )
        session.add(data)

        update_balance = (
            update(acc)
            .where(acc.account_number == data.account_number)
            .values(balance=balance_amount)
        )

        await session.execute(update_balance)
        await session.commit()
        return response(
            "200",
            f"Tarik tunai dengan nomor rekening {data.account_number} berhasil, saldo sekarang Rp. {balance_amount}",
            balance_amount,
        )
    except Exception as e:
        return response("400", str(e), None)


async def balance_account(account_number: str, session) -> response:
    try:
        acc = account_model.Account
        query_stmt = select(acc.balance).where(acc.account_number == account_number)
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()

        return response(
            "200",
            f" Saldo Anda sekarang Rp.{res}",
            res,
        )

    except Exception as e:
        return response("400", str(e), None)


async def acc_exist(account_number: str, session) -> response:
    try:
        acc = account_model.Account
        query_stmt = select(acc.account_number).where(
            acc.account_number == account_number
        )
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)
    except Exception as e:
        return response("400", str(e), None)


async def valAccExist(nik: str, session) -> response:
    try:
        acc = account_model.Account
        query_stmt = select(acc).where(acc.no_identity == nik)
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().first()
        return jsonable_encoder(res)
    except Exception as e:
        return response("400", str(e), None)


async def mutation(account_number: str, mutation: str, session) -> response:
    try:
        trx = transaction_model.Transaction

        terms = []
        if mutation not in (None, ""):
            if mutation.upper() not in ("D", "C"):
                raise Exception(
                    "Jenis mutasi tidak valid, pilih mutasi C (Credit) atau D (Debit)."
                )
        if mutation not in (None, ""):
            terms.append(or_(trx.mutation.ilike(f"%{mutation.upper()}%")))
        if len(terms) > 0:
            query_stmt = (
                select(trx)
                .filter(and_(*(terms), trx.account_number == account_number))
                .order_by(desc(trx.created_at))
            )
        else:
            query_stmt = (
                select(trx)
                .where(trx.account_number == account_number)
                .order_by(desc(trx.created_at))
            )
        proxy = await session.execute(query_stmt)
        res = proxy.scalars().all()

        if res in ([], None):
            raise Exception("Data mutasi tidak ditemukan")

        data = []
        for i in res:
            dt = {
                "account_number": i.account_number,
                "transactions_date": i.created_at,
                "code_transactions": "withdraw" if i.mutation == "D" else "Deposit",
                "type": i.type,
                "nominal": i.nominal
            }
            data.append(dt)
        return data
    except Exception as e:
        return response("400", str(e), None)
